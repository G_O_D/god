# GOD

bahasa pemrograman berbahasa indonesia

DEVELOPER
1. Muhammad Quwais Safutra
2. Ahmad Frendi
3. Timo
4. Ilalang Kering
5. yana sulyana

Dokumen Ini Ditulis Oleh : Muhammad Quwais Safutra

tutorial

1. Print Teks

#include "god.h"
kelas sistem(){
    cetak<<"halo dunia\n";
    yn();
    menuju 0;
}

NOTE: yn digunakan untuk menghentikan proses
-----------------------------------------------------------------------------
2. Get User Input

#include "god.h"

kelas sistem(){
    str x;
    baca>>x;
    cetak<<x<<"\n";
    menuju 0;
}
-----------------------------------------------------------------------------
3. Loop 1->5

#include "god.h"

kelas sistem(){
    loop(1,5);
    menuju 0;
}
-----------------------------------------------------------------------------
5. loop Berbaris baru

#include "god.h"

kelas sistem(){
    loop_baris_baru(1,5);
    menuju 0;
}